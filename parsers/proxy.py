import random
def proxy_rotator():
    with open('proxy.txt') as f:
        proxies = f.readlines()
    if proxies:
        return random.choices(proxies)[0].strip()
    else:
        return None