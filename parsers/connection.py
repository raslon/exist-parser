import requests
import fake_useragent
from stem import Signal
from stem.control import Controller

from parsers.proxy import proxy_rotator


class Connection():
    def __init__(self):
        ua = fake_useragent.UserAgent()
        self.headers = {'User-Agent': str(ua.random)}
        self.session = requests.Session()
        self.proxy = proxy_rotator()
        # with Controller.from_port(port=9051) as c:
        #     c.authenticate(password='my_password')
        #     print("Success!")
        #     c.signal(Signal.NEWNYM)
        #     print("New Tor connection processed")

    def get_html(self, link, cookies=None, header: dict = None):


        if header:
            self.headers.update(header)
        self.session.headers.update(self.headers)
        # if self.proxy:
        #     self.session.proxies = {'http': 'http://{}'.format(self.proxy), 'https': 'https://{}'.format(self.proxy)}
        self.session.proxies = {'http': 'http://127.0.0.1:8118',}
        print(self.session.get("http://httpbin.org/ip").text)
        if cookies:
            sess = self.session.get(link, cookies=cookies, timeout=30)  # ,
        else:
            sess = self.session.get(link, timeout=30)
        return sess.text

    def __enter__(self):
        # print("__enter__")
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        # print("__exit__")
        self.session.close()