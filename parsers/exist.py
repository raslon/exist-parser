import csv
import json
import os
import random
import re
import time
import unicodedata
from io import StringIO, BytesIO
from functools import partial
from multiprocessing import Pool

from .connection import *
import bs4


class Exist(Connection):
    bs = partial(bs4.BeautifulSoup, features='lxml')
    base_url = 'https://exist.ru'
    data_re = re.compile(r'var\ \_data\ \=\ (.+)\;\ var', re.MULTILINE)
    # Cookies = {'_ref': 'https://kwork.ru/inbox/egor806'}
    Cookies = None

    def write_csv(self, data, file_name):
        with open('{}.csv'.format(file_name), 'a', encoding='utf-8') as f:
            writer = csv.writer(f)
            writer.writerow((data['CatalogName'], data['PartNumber'], data['Description'], data['price']))

    def get_links(self, link):
        links = []
        with Connection() as connect:
            html = connect.get_html(link, cookies=self.Cookies)
            soup = self.bs(html)
            tree_ = soup.find('ul', {'class': 'tree'}).find_all('a')
            for tree in tree_:
                links.append('{}{}'.format(self.base_url, tree['href']))
        return links

    def product_search(self, link):
        links = []
        with Connection() as connect:
            try:
                html = connect.get_html(link, cookies=self.Cookies)
                soup = self.bs(html)
                tree_ = soup.find('table', {'class': 'tbl'}).find_all('a')
                for tree in tree_:
                    print(tree['href'])
                    time.sleep(random.uniform(2,10))
                    self.get_data('{}{}'.format(self.base_url, tree['href']))
                    # links.append('{}{}'.format(self.base_url, tree['href']))
            except:
                print('ERROR: ', link)
        # print(links)
        # return links

    def get_data(self, link):
        data = []
        product = {}
        with Connection() as connect:
            html = connect.get_html(link, cookies=self.Cookies)
            soup = self.bs(html)
            raw_data = soup.find('script', text=self.data_re)
            getData = json.loads(self.data_re.search(str(raw_data))[1])[0]
            product['CatalogName'] = getData['CatalogName']
            product['PartNumber'] = getData['PartNumber']
            product['Description'] = getData['Description']

            AggregatedParts = getData['AggregatedParts']
            if AggregatedParts:
                for AggregatedPart in AggregatedParts:
                    product['price'] = str(AggregatedPart['price'])
                    self.write_csv(product, 'File')
                    data.append(product)
            else:
                product['price'] = 'Временно нет в продаже'
                data.append(product)
                self.write_csv(product, 'File')
            # return data

    def start(self, link):

        #self.get_data('https://exist.ru/Price/?pid=1CF0D3EB&sr=29&cn=Renault_Captur_0.9_TCe_90_')
        #self.get_data('https://exist.ru/Price/?pid=4BA0E237&sr=29&cn=Renault_Captur_0.9_TCe_90_(H4B_400)')
        #self.get_data('https://exist.ru/Price/?pid=5880A3E0&sr=29&cn=Renault_Captur_0.9_TCe_90_')
        #self.get_data('https://exist.ru/Price/?pid=53E0E237&sr=29&cn=Renault_Captur_0.9_TCe_90_(H4B_400)')
        detal_links = self.get_links(link)

        print(len(detal_links))
        # links = []
        for i, detal_link in enumerate(detal_links):
            print(i, detal_link)
            # time.sleep(random.uniform(2,10))
            self.product_search(detal_link)
            # time.sleep(random.uniform(20,40))
